import { createSlice } from '@reduxjs/toolkit';
// import DemoImage from 'assets/images/demoImage.png';
import { Dispatch } from 'redux';
// import { Login } from 'api/Auth';
// import { getUserListAssets } from 'api/Assets';
// import { CredInfoProps, CredInfo } from 'api/Interface';

type PropsData = {
    img: string;
    title: string;
    id: number;
};

const data: Array<PropsData> = [];

for (let i = 0; i < 30; i++) {
    data.push({
        img: '',
        title: `[Asset ${i} Name]`,
        id: i,
    });
}

export const testSlice = createSlice({
    name: 'test',
    initialState: {
        tileData: data,
        /**
         * Параметр выбора сервера
         */
        serverName: '',
        /**
         * Параметр выбора одиночного шаблона
         */
        singlePage: '',
        /**
         * крипто ключь пользователя
         */
        user: {
            userId: '',
            prvKey: '',
            pubKey: '',
        },
    },
    reducers: {
        changeServerName: (state: any, action: any): void => {
            state.serverName = action.payload;
        },
        setSinglePage: (state: any, action: any): void => {
            state.singlePage = action.payload;
        },
        setUser: (state: any, { payload: { userId, prvKey, pubKey } }): void => {
            state.user = {
                userId,
                prvKey,
                pubKey,
            };
        },
        dischargeUser: (state: any): void => {
            state.user = {
                userId: '',
                prvKey: '',
                pubKey: '',
            };
        },
    },
});

export const { changeServerName, setSinglePage, setUser } = testSlice.actions;

// eslint-disable-next-line @typescript-eslint/explicit-module-boundary-types
export const changeServerNameAsync = (serverName: string) => async (dispatch: Dispatch) => {
    // dispatch(changeServerName(serverName));
    try {
        // const data = await Login(serverName);
        // await dispatch(setUser(data));
        // await getUserListAssets({ ...(data as CredInfo), serverName } as CredInfoProps);
        // console.log(answerDeals);
    } catch (err) {
        console.log(err);
    }
};

// eslint-disable-next-line @typescript-eslint/explicit-module-boundary-types
export const tileData = (state: any) => state.gallery.tileData;
// eslint-disable-next-line @typescript-eslint/explicit-module-boundary-types
export const getServerName = (state: any) => state.gallery.serverName;
// eslint-disable-next-line @typescript-eslint/explicit-module-boundary-types
export const singlePage = (state: any) => state.gallery.singlePage;
// eslint-disable-next-line @typescript-eslint/explicit-module-boundary-types
export const singleTileData = (state: any) => state.gallery.tileData?.[state.gallery.singlePage] || {};
/**
 * Перобразовываем входящий параметр в булевое значение
 * для выбора одиночной картинки и переключения шаблона
 */
// eslint-disable-next-line @typescript-eslint/explicit-module-boundary-types
export const isSinglePage = (state: any) => Number.isInteger(state.gallery.singlePage);
// eslint-disable-next-line @typescript-eslint/explicit-module-boundary-types
export const getUserParams = (state: any) => state.gallery.user;

export default testSlice.reducer;
