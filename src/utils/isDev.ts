// eslint-disable-next-line @typescript-eslint/explicit-module-boundary-types
const isDev = process.env.NODE_ENV === 'development';

export default isDev;
